/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}","./node_modules/flowbite/**/*.js"
  ],
  theme: {
    extend: {
      myfont:["font-family: 'Tilt Warp', cursive;"]
    },
  
  },
  plugins: [
    require('flowbite/plugin')
]
}