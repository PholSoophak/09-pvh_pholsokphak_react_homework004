import React, { Component } from "react";
import DisplayInput from "./DisplayInput";



export default class InputFromUser extends Component {


  constructor() {
    super();
    this.state = {
      studentList: [
        { id: 1, stuName: "Sophak", email: "sophakphol@gmail.com ", age: 23,status:"Pending" },
      ],
      newStudentName: "null",
      stuEmail: "null",
      stuAge: "null",
      // status:"Pending"
      
    };
  }
  handleChangeStuName = (event) => {
    this.setState({
      newStudentName: event.target.value,
    });
  };

  handleChangeStuEmail = (event) => {
    this.setState({
      stuEmail: event.target.value,
    });
  };

  handleChangeAge = (event) => {
    this.setState({
      stuAge: event.target.value,
    });
  };

  onSubmit = () => {
    // console.log(this.state.stuName.length);
    const newStuObj = {
      id: this.state.studentList.length + 1,
      stuName: this.state.newStudentName,
      email: this.state.stuEmail,
      age: this.state.stuAge,
      status:"Pending"
    };
    this.setState({
      studentList: [...this.state.studentList, newStuObj],
      newStudentName: "null",
      stuEmail: "null",
      stuAge: "null",
      status:"Pending"
    });
    // ,()=>console.log("New Arr Student name", this.state.studentList)
    }
    changeBtn = (obj) =>{
    // let std=this.state.studentList;
    // console.log(std);
    obj.status==="Pending" ? obj.status = "Done" 
    : obj.status = "Pending";
     this.setState({
      studentList:[...this.state.studentList]
     })

     console.log(this.state.studentList);
  };

  render() {
    return (
      <div className="bg-[#4E6E81] p-20">
      <div className="p-10 m-auto max-w-3xl">
        <h1 className="text-4xl font-extrabold  bg-clip-text text-[#bfdbe5] text-center ">
          Student Register
        </h1>
        <div class="mb-4  flex flex-col gap-y-5 mt-10">
          {/* <label
            class="block text-gray-700 text-sm font-bold   "
            for="username"
          >
            Student Name
          </label> */}
          <input
            class="shadow appearance-none border  w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline h-[55px]"
            id="username"
            type="text"
            placeholder="Student name"
            onChange={this.handleChangeStuName}
          />
                    {/* <label
            class="block text-gray-700 text-sm font-bold   "
            for="username"
          >
            Student Email
          </label> */}
          <input
            class="shadow appearance-none border  w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline h-[55px]"
            id="username"
            type="text"
            placeholder="Email"
            onChange={this.handleChangeStuEmail}
          />

          <input
            class="shadow appearance-none border  w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline h-[55px]"
            type="text"
            placeholder="Age"
            onChange={this.handleChangeAge}
          />
        </div>

        <button
          onClick={this.onSubmit}
          class="bg-gradient-to-r from-green-400 to-blue-500 hover:from-pink-500 hover:to-yellow-500 text-white font-bold py-2 px-4 rounded m-auto"
        >
          Register
        </button>

        {/* Table user Name */}

      </div>

        <DisplayInput data={this.state.studentList} changeBtn = {this.changeBtn}/>
      </div>
    );
  }
}
