import React, { Component } from "react";
import Swal from 'sweetalert2'

export default class DisplayInput extends Component {


  
  showAlert (obj){
    Swal.fire({
      title:" ID:"+ `${obj.id}`+"<br> Name:"+`${obj.stuName}`
             +"<br> Email:"+`${obj.email}`+"<br> Student age:"+`${obj.age}`, 
             showClass: {
              popup: 'animate__animated animate__fadeInDown'
            },
            hideClass: {
              popup: 'animate__animated animate__fadeOutUp'
            }
    })
  }

  render() {
    return (
      <div className="max-w-full  m-auto">
        <h1 className="text-4xl font-extrabold text-[#bfdbe5] text-center drop-shadow-xl mt-5">
          Display Student Information
        </h1>
        <div class="relative overflow-x-auto mt-10 ">
          <table class="w-full text-[16px] text-left text-gray-800">
            <thead class="text-md text-gray-700 uppercase bg-gray-50">
              <tr>
                <th scope="col" class="px-6 py-3">
                  ID
                </th>
                <th scope="col" class="px-6 py-3">
                  Student Name
                </th>
                <th scope="col" class="px-6 py-3">
                 student Email
                </th>
                <th scope="col" class="px-6 py-3">
                  student age
                </th>
                <th scope="col" class="px-6 py-3 text-center">
                  Action
                </th>
              </tr>
            </thead>
            <tbody>
              {this.props.data.map((item) => (
                <tr class="bg-white border-b even:bg-[#FF5F9E] odd:bg-[#F0EEED]">
                  <td class="px-6 py-4">{item.id}</td>
                  <td class="px-6 py-4">{item.stuName}</td>
                  <td class="px-6 py-4">{item.email}</td>
                  <td class="px-6 py-4">{item.age}</td>

                  <td>
                    <button
                      type="button"
                      class={item.status==="Pending" ? 'text-white bg-red-600 hover:bg-gradient-to-bl focus:ring-4 focus:outline-none focus:ring-blue-300  font-medium rounded-lg text-sm px-5 py-2.5 text-center mr-2 mb-2 ml-20 mt-5 p-5 w-[100px]': 'text-white bg-green-600 hover:bg-gradient-to-bl focus:ring-4 focus:outline-none focus:ring-blue-300  font-medium rounded-lg text-sm px-5 py-2.5 text-center mr-2 mb-2 ml-20 mt-5 w-[100px]'}
                      onClick={()=>this.props.changeBtn(item)}

                    >
                      {item.status}
                    </button>
                    <button
                      
                      type="button"
                      class="text-white bg-blue-600 hover:bg-gradient-to-l focus:ring-4 focus:outline-none focus:ring-purple-200  font-medium rounded-lg text-sm px-5 py-2.5 text-center mr-2 mb-2  "
                      onClick={()=>this.showAlert(item)}
                    >
                    Show More
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}
